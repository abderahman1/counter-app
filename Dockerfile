FROM alpine

COPY . /app/

RUN apk update && apk add py3-pip python3

WORKDIR /app

RUN pip install -r requirements.txt

ENTRYPOINT [ "python3", "app.py" ]

